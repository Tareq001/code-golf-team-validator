//
//  ContentView.swift
//  TeamValidator
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//
//

import SwiftUI

struct ContentView: View {
    @State var showUnbalancedTeamsOnly = false
    let teamCompositions: [[String]] = [
        ["Dreamers", "Driver", "Analytical", "Amiable"],
        ["Blueberries", "Driver", "Analytical", "Amiable", "Expressive", "Driver", "Analytical", "Amiable", "Expressive"],
        ["Tramps", "Analytical", "Amiable", "Expressive"],
        ["Butterflies", "Driver", "Analytical", "Amiable", "Analytical", "Amiable", "Expressive"],
        ["Coffee Lovers", "Driver", "Analytical", "Expressive", "Driver", "Analytical", "Expressive"],
        ["Dolphins", "Driver", "Analytical", "Amiable", "Expressive", "Analytical", "Amiable"],
        ["Dream Team", "Amiable", "Expressive", "Amiable", "Expressive"],
        ["Gazelles", "Driver", "Amiable", "Expressive"],
        ["The Fantastic Four", "Driver", "Analytical", "Amiable", "Expressive"],
        ["Unicorns", "Analytical", "Amiable", "Expressive"]
    ]
    
    var body: some View {
        NavigationView {
            List() {
                ForEach(getVisibleTeamCompositions(), id: \.self) { teamComposition in
                    Section(header: Text("Team: \(teamComposition.first ?? "-")")) {
                        ForEach(teamComposition.dropFirst(), id: \.self) { socialStyle in
                            Text(socialStyle)
                        }
                    }
                }
            }
            .navigationBarTitle("Teams")
            .navigationBarItems(
                trailing: Toggle(isOn: $showUnbalancedTeamsOnly) {
                    Text("unbalanced only")
                }
            )
        }
    }
    
    func getVisibleTeamCompositions() -> [[String]] {
        let unbalancedTeams = findUnbalancedTeams(teamCompositions)
        
        return teamCompositions.filter { (teamComposition: [String]) in
            !showUnbalancedTeamsOnly || unbalancedTeams.contains(teamComposition.first ?? "")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

